import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

// Extend the Request interface to include the user property
interface AuthenticatedRequest extends Request {
  user?: any; // Update with the actual type of your user object
}

@Injectable()
export class JwtAuthGuard implements CanActivate {
  constructor(private readonly jwtService: JwtService) {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<AuthenticatedRequest>();

    try {
      const jwtCookie = request.cookies['jwt'];

      if (!jwtCookie) {
        return false;
      }

      const decoded = this.jwtService.verify(jwtCookie);
      request.user = decoded;
      return true;
    } catch (error) {
      return false;
    }
  }
}
