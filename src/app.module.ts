import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {User} from "./user/user.entity";
import { Test } from './test/test.entity';
import { AuthModule } from './auth/auth.module';
import { TestModule } from './test/test.module';


@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'root',
            password: '',
            database: 'bl',
            entities: [User,Test],
            synchronize: true,
        }),
        
        AuthModule,
        TestModule
    ],
    
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
