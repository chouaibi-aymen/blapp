import { Controller, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { TestService } from './test.service';
import { Test } from './test.entity';
import * as ExcelJS from 'exceljs';
import { Multer } from 'multer';

@Controller('api/tests')
export class TestController {
  constructor(private readonly testService: TestService) {}

  @Post('import')
@UseInterceptors(FileInterceptor('file'))
async importTestData(@UploadedFile() file: Multer.File) {
  try {
    const tests: Test[] = await parseExcelBuffer(file.buffer);

    const savedTests = await Promise.all(tests.map(test => this.testService.create(test)));

    return { message: 'Import successful', data: savedTests };
  } catch (error) {
    console.error('Error importing data:', error.message);
    throw error;
  }
}
}
async function parseExcelBuffer(buffer: Buffer): Promise<Test[]> {
  try {
    const workbook = new ExcelJS.Workbook();
    const tests: Test[] = [];

    await workbook.xlsx.load(buffer);
    const worksheet = workbook.getWorksheet(1);

    worksheet.eachRow((row, rowNumber) => {
      if (rowNumber !== 1) {
        const test: Test = {
          username: row.getCell(1).toString(),
          email: row.getCell(2).toString(),
          id: 0,
        };

        tests.push(test);
      }
    });

    return tests;
  } catch (error) {
    console.error('Error parsing Excel buffer:', error.message);
    throw error;
  }
}

  
  
