import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nom: string;

    @Column()
    matriculeFiscale: string;

    @Column()
    phoneNumber: number;

    @Column()
    gover: string;

    @Column({ unique: true })
    email: string;

    @Column() 
    logo: string;

    @Column()
    password: string;

    @Column()
    fraisLivraison: string;

    @Column({ nullable: true })
    resetCode: number;
}